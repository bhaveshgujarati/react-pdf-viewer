<p align="center"><img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>

## Laravel 8 and React JS

Please follow the guide.

## Prerequisite

1. Make sure you have [composer](https://getcomposer.org/download/) installed.
2. Make sure you have latest stable version of [node](https://nodejs.org/en/download/) installed.

### Option 1

1. `git clone`
2. `create a .env file copy content from .env.example and update the values`
3. `composer install`
4. `php artisan key:gen`
5. `php artisan migrate`
6. `npm install && npm run dev`
7. `php artisan serve`


