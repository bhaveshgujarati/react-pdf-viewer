<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Document;
use App\Models\Media;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class DocumentController extends Controller
{
    public function documents()
    {
        $documnets = Document::all();
        return response(['message' => "Documents Loaded!", 'data' => $documnets]);
    }
    public function upload(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'file' => 'required|mimes:pdf',
        ]);
        if ($validator->fails()) {
            return response(['status' => 'error', 'message' => $validator->errors()->first()], 421);
        }

        $original_pic     = $request->file('file');
        $fileSize         = $original_pic->getSize();
        $fileExt          = $request->file('file')->getClientOriginalExtension();
        if($fileExt == 'PDF'){
            $fileExt = 'pdf';
        }
        $fileName         = time() . '-' . rand(0000, 9999) . '-document';
        $fileNameToStore  = $fileName . "." . $fileExt;
        $fileOriginName   = $request->file('file')->getClientOriginalName();

        $data = Document::create([
            'title' => $fileOriginName,
            'file' => $fileNameToStore,
            'ext' => $fileExt,
            'size' => $fileSize,
        ]);
        if(!$data){
            return response(['status' => 'error', 'message' => 'file not uploaded!'], 421);
        }

        $request->file('file')->storeAs('public/document', $fileNameToStore);

        return response(['message' => "Document has been successfully uploaded.", 'data' => $data]);
    }
}
