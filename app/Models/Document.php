<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'size',
        'file',
        'ext',
    ];

    protected $appends = ['url'];

    public function getUrlAttribute()
    {
        return asset('storage/document/'.$this->file);
    }
}
