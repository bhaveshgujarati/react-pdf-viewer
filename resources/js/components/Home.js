import React, { Component, useState, useEffect } from 'react';

import Header from './Header'; //Include Heder
// import Footer from './Footer'; //Include Footer
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
// import { PDFReader } from 'reactjs-pdf-reader';
import { Document, Page , pdfjs} from 'react-pdf';
pdfjs.GlobalWorkerOptions.workerSrc = `//cdnjs.cloudflare.com/ajax/libs/pdf.js/${pdfjs.version}/pdf.worker.js`;

function MyApp(url) {
    const [numPages, setNumPages] = useState(null);
    const [pageNumber, setPageNumber] = useState(1);

    function onDocumentLoadSuccess({ numPages }) {
        setNumPages(numPages);
    }

    return (
        <div>
            <Document
                file={url}
                onLoadSuccess={onDocumentLoadSuccess}
            >
                <Page pageNumber={pageNumber} />
            </Document>
            <p>Page {pageNumber} of {numPages}</p>
        </div>
    );
}

class Home extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            documents: [],
            url: '',
            isShow: false,
            headerTitle : 'Document #1',
            active: '',
        };
        this.changeDocument = this.changeDocument.bind(this);
    }

    async componentDidMount() {
        await this.getImages();
    }

    componentDidUpdate() {
        // this.setState = {
        //     url: this.state.documents[0].url,
        // };
    }
    handleClick(e) {
        $("#file").click();
    }
    async changeDocument(document,key){
        await this.setState({ url: document.url, headerTitle: 'Document #'+key, active: document.id, });
    }
    getImages = (isUploadNew=false) => {
        axios
            .get("/api/documents")
            .then((response) => {
                if (response.status === 200) {
                    this.setState({
                        documents: response.data.data,
                        url: response.data.data[0].url,
                        headerTitle : 'Document #1',
                        active : response.data.data[0].id
                    });
                }
            })
            .catch((error) => {
                console.error(error);
            });
    };
    // file validation
    fileValidate = (file) => {
        if (
            file.type === "application/pdf"
        ) {
            return true;
        } else {
            return false;
        }
    };
    submitHandler = (e) => {
        e.preventDefault();
        const data = new FormData();

        const valid = this.fileValidate(e.target.files[0]);
        if (!valid){
            toast.error("File type allowed only pdf");
            return false;
        }

        data.append('file', e.target.files[0]);
        var $this = this;

        axios.post("/api/document/upload", data)
            .then((response) => {
                if (response.status === 200) {
                    var joined = this.state.documents.concat(response.data.data);
                    this.setState({ documents: joined });
                }
            })
            .catch((error) => {
                //console.error(error);
            });
    };

    render() {
        var url = this.state.url;

        return (
            <div className="d-flex" id="wrapper">
                <ToastContainer />
                <div className="border-end" id="sidebar-wrapper">
                    <div className="sidebar-heading border-bottom bg-light">FILES <button onClick={this.handleClick.bind(this)} type="button"
                                                                                                    className="btn btn-outline-secondary btn-sm upload-btn">
                        Upload
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                             className="bi bi-upload" viewBox="0 0 16 16">
                            <path
                                d="M.5 9.9a.5.5 0 0 1 .5.5v2.5a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1v-2.5a.5.5 0 0 1 1 0v2.5a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2v-2.5a.5.5 0 0 1 .5-.5z"></path>
                            <path
                                d="M7.646 1.146a.5.5 0 0 1 .708 0l3 3a.5.5 0 0 1-.708.708L8.5 2.707V11.5a.5.5 0 0 1-1 0V2.707L5.354 4.854a.5.5 0 1 1-.708-.708l3-3z"></path>
                        </svg>
                        <input onChange={this.submitHandler} type="file" id="file" style={{display: "none"}}/>
                        <span className="visually-hidden"></span>
                    </button></div>
                    <div className="list-group list-group-flush">
                        {
                            this.state.documents.length > 0 ? (
                                this.state.documents.map((document, key) => (
                                    <span onClick={() => this.setState({ url: document.url })} onClick={() => this.changeDocument(document,key+1)} key={document.id} className={`list-group-item list-group-item-light p-4 ${document.id == this.state.active ? 'list-group-item-action' : ''}`} >Document #{key + 1}</span>
                                ))
                            ) : (
                                <span className="list-group-item list-group-item-action list-group-item-light p-4 text-danger text-center">No Document Found </span>
                            )
                        }
                    </div>
                </div>

                <div id="page-content-wrapper">
                    <nav className="navbar navbar-expand-lg navbar-light border-bottom" style={{'height': '60px'}}>
                        <div className="container-fluid">
                            <span className="text-white">{this.state.headerTitle}</span>
                        </div>
                    </nav>
                    <div className="container-fluid">
                        <div className="main-container">
                            {
                                this.state.documents.length > 0 ? (
                                    <MyApp url={url}/>
                                ) : (
                                    <span className="list-group-item list-group-item-action list-group-item-light p-4 text-danger text-center">No Document Found </span>
                                )
                            }
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Home;
